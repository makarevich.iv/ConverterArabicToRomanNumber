package org;

public class RomanNumber {
    private String letter;
    private int value;
    private int repeated;

    RomanNumber(String letter, int value, int repeated) {
        this.value = value;
        this.repeated = repeated;
        this.letter = letter;
    }

    public String getLetter() {
        return letter;
    }

    public int getValue() {
        return value;
    }

    public int getRepeated() {
        return repeated;
    }
}