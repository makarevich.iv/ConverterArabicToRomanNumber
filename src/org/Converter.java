package org;

import java.util.ArrayList;
import java.util.List;

public class Converter {
    final private static List<RomanNumber> listRomanNumber = new ArrayList<>();
    final private static int NEVER_REPEAT = 1;
    final private static int REPEAT_ATMOST_3_TIMES = 3;
    final private static RomanNumber ROMAN_NUMBER_I =  new RomanNumber("I",    1,      REPEAT_ATMOST_3_TIMES);
    final private static RomanNumber ROMAN_NUMBER_X =  new RomanNumber("X",    10,     REPEAT_ATMOST_3_TIMES);
    final private static RomanNumber ROMAN_NUMBER_C =  new RomanNumber("C",    100,    REPEAT_ATMOST_3_TIMES);
    final private static RomanNumber ROMAN_NUMBER_M =  new RomanNumber("M",    1000,   REPEAT_ATMOST_3_TIMES);
    final private static RomanNumber ROMAN_NUMBER_V =  new RomanNumber("V",    5,      NEVER_REPEAT);
    final private static RomanNumber ROMAN_NUMBER_L =  new RomanNumber("L",    50,     NEVER_REPEAT);
    final private static RomanNumber ROMAN_NUMBER_D =  new RomanNumber("D",    500,    NEVER_REPEAT);
    final private static RomanNumber ROMAN_NUMBER_IV = new RomanNumber("IV",   4,      NEVER_REPEAT);
    final private static RomanNumber ROMAN_NUMBER_IX = new RomanNumber("IX",   9,      NEVER_REPEAT);
    final private static RomanNumber ROMAN_NUMBER_XL = new RomanNumber("XL",   40,     NEVER_REPEAT);
    final private static RomanNumber ROMAN_NUMBER_XC = new RomanNumber("XC",   90,     NEVER_REPEAT);
    final private static RomanNumber ROMAN_NUMBER_CD = new RomanNumber("CD",   400,    NEVER_REPEAT);
    final private static RomanNumber ROMAN_NUMBER_CM = new RomanNumber("CM",   900,    NEVER_REPEAT);


    static {
        listRomanNumber.add(ROMAN_NUMBER_I);
        listRomanNumber.add(ROMAN_NUMBER_X);
        listRomanNumber.add(ROMAN_NUMBER_C);
        listRomanNumber.add(ROMAN_NUMBER_M);
        listRomanNumber.add(ROMAN_NUMBER_V);
        listRomanNumber.add(ROMAN_NUMBER_L);
        listRomanNumber.add(ROMAN_NUMBER_D);
        listRomanNumber.add(ROMAN_NUMBER_IV);
        listRomanNumber.add(ROMAN_NUMBER_IX);
        listRomanNumber.add(ROMAN_NUMBER_XL);
        listRomanNumber.add(ROMAN_NUMBER_XC);
        listRomanNumber.add(ROMAN_NUMBER_CD);
        listRomanNumber.add(ROMAN_NUMBER_CM);
    }

    private static void checkValidRomanNumber(String romanInput) {
        RomanNumber previousRomanNumber = new RomanNumber("ZZZ", 1000_000, NEVER_REPEAT);
        int countRepeatedRomanNumber = 1;
        for(int i = 0; i < romanInput.length(); i++) {
            RomanNumber romanNumber = getNextRomanNumber(romanInput, i);
            if (previousRomanNumber.getValue() == 9 || previousRomanNumber.getValue() == 4)
                throw new IllegalArgumentException("Wrong roman string. Last roman number must ending 'I', 'IV' or 'IX'");
            if (previousRomanNumber.getValue() <= romanNumber.getValue()) {
                if (previousRomanNumber.getValue() == romanNumber.getValue()) {
                    countRepeatedRomanNumber++;
                    if (romanNumber.getRepeated() < countRepeatedRomanNumber) {
                        throw new IllegalArgumentException("Wrong roman string. Roman Number:" + romanNumber.getLetter() + " (" + romanNumber.getValue() + ") repeated more than " + romanNumber.getRepeated() + " times: " + countRepeatedRomanNumber);
                    }
                } else {
                    throw new IllegalArgumentException("Wrong roman string. Next Roman Nubmber: " +
                            romanNumber.getLetter() + " (" + romanNumber.getValue() + ") greater than previous Roman Number: " +
                            previousRomanNumber.getLetter() + " (" + previousRomanNumber.getValue() + ")");
                }
            } else {
                countRepeatedRomanNumber = 1;
            }
            if (romanNumber.getLetter().length() == 2) i++;
            previousRomanNumber = romanNumber;
        }
    }

    private static RomanNumber getNextRomanNumber(String romanInput, int pos) {
        RomanNumber romanNumber = null;
        if (pos != romanInput.length() - 1)
            romanNumber = getItemFromListRomanNumber(romanInput.substring(pos, pos + 2));
        if (romanNumber == null)
            romanNumber = getItemFromListRomanNumber(romanInput.substring(pos, pos + 1));
        return romanNumber;
    }

    public static int convertRomanToArabic(String romanInput) {
        checkValidRomanNumber(romanInput);
        int sum = 0, countRomanNumber;
        for(int i = 0; i < romanInput.length(); i++) {
            RomanNumber romanNumber = getNextRomanNumber(romanInput, i);
            if (NEVER_REPEAT == romanNumber.getRepeated()) {
                sum = sum + romanNumber.getValue();
                if (romanNumber.getLetter().length() == 2) i++;
                continue;
            }
            countRomanNumber = getCountRomanNumber(romanInput, romanNumber, i);
            if (countRomanNumber > romanNumber.getRepeated())
                throw new IllegalArgumentException("Illegal input string. Roman Letter '" + romanNumber.getLetter() +
                        "' repeated more than " + romanNumber.getRepeated() + " times: " + countRomanNumber);
            sum = sum + countRomanNumber * romanNumber.getValue();
            i = i + countRomanNumber - 1;
        }
        return sum;
    }

    public static String convertArabicToRoman(int arabicNumber) {
        StringBuilder result = new StringBuilder();
        int temp;

        // 1000
        temp = arabicNumber / ROMAN_NUMBER_M.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_M.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_M.getValue();

        // 900
        temp = arabicNumber / ROMAN_NUMBER_CM.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_CM.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_CM.getValue();

        // 500
        temp = arabicNumber / ROMAN_NUMBER_D.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_D.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_D.getValue();

        // 400
        temp = arabicNumber / ROMAN_NUMBER_CD.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_CD.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_CD.getValue();

        // 100
        temp = arabicNumber / ROMAN_NUMBER_C.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_C.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_C.getValue();

        // 90
        temp = arabicNumber / ROMAN_NUMBER_XC.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_XC.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_XC.getValue();

        // 50
        temp = arabicNumber / ROMAN_NUMBER_L.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_L.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_L.getValue();

        // 40
        temp = arabicNumber / ROMAN_NUMBER_XL.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_XL.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_XL.getValue();

        // 10
        temp = arabicNumber / ROMAN_NUMBER_X.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_X.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_X.getValue();

        // 9
        temp = arabicNumber / ROMAN_NUMBER_IX.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_IX.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_IX.getValue();

        // 5
        temp = arabicNumber / ROMAN_NUMBER_V.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_V.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_V.getValue();

        // 4
        temp = arabicNumber / ROMAN_NUMBER_IV.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_IV.getLetter());
        arabicNumber = arabicNumber - temp * ROMAN_NUMBER_IV.getValue();

        // 1
        temp = arabicNumber / ROMAN_NUMBER_I.getValue();
        if (temp > 0)
            for (int i = 0; i < temp; i++) result.append(ROMAN_NUMBER_I.getLetter());

        return result.toString();
    }

    private static RomanNumber getItemFromListRomanNumber(String str) {
        RomanNumber result = null;
        for(RomanNumber romanNumber: listRomanNumber) {
            if (romanNumber.getLetter().equals(str))
                result = romanNumber;
        }
        return result;
    }

    private static int getCountRomanNumber(String romanInput, RomanNumber romanNumber, int pos) {
        int count = 0;
        while (pos < romanInput.length()) {
            if (romanNumber.getLetter().equals(romanInput.substring(pos, ++pos))) {
                count++;
            } else {
                break;
            }
        }
        return count;
    }
}