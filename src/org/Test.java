package org;

public class Test {
    public static void main(String[] args) {
        String romanInput = "IXCX";  //"cmxcix"; //999    //"MMCMLXXXVI"; // 2986
        int arabicNumber;

        System.out.println("Input string: " + romanInput);

        arabicNumber = Converter.convertRomanToArabic(romanInput.toUpperCase());
        System.out.println("'" + romanInput.toUpperCase() + "' convert to arabic: " + arabicNumber);

        String romanNumber = Converter.convertArabicToRoman(arabicNumber);
        System.out.println("Arabic '" + arabicNumber + "' convert to roman number: " + romanNumber);
    }
}
